<?php

namespace Dent\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;

class PacienteTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll() {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function fetchByName($nome) {
        $where = new Where();
        $where->like('primeiro_nome', $nome.'%');
        $resultSet = $this->tableGateway->select($where);
        return $resultSet;
    }

    public function getPaciente($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function savePaciente(Paciente $paciente) {
        $data = array(
            'data_cadastro' => $paciente->data_cadastro,
            'primeiro_nome' => $paciente->primeiro_nome,
            'sobrenome' => $paciente->sobrenome,
            'data_nascimento' => $paciente->data_nascimento,
        );

        $id = (int) $paciente->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getPaciente($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Paciente id does not exist');
            }
        }
    }

    public function deletePaciente($id) {
        $this->tableGateway->delete(array('id' => (int) $id));
    }

}
