<?php

namespace Dent\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Paciente implements InputFilterAwareInterface {

    public $id;
    public $data_cadastro;
    public $primeiro_nome;
    public $sobrenome;
    public $data_nascimento;
    
    protected $inputFilter;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->data_cadastro = (!empty($data['data_cadastro'])) ? $data['data_cadastro'] : null;
        $this->primeiro_nome = (!empty($data['primeiro_nome'])) ? $data['primeiro_nome'] : null;
        $this->sobrenome = (!empty($data['sobrenome'])) ? $data['sobrenome'] : null;
        $this->data_nascimento = (!empty($data['data_nascimento'])) ? $data['data_nascimento'] : null;
        
    }
    
     public function getArrayCopy()
     {
         return get_object_vars($this);
     }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add(array(
                'name' => 'id',
                'required' => true,
                'filters' => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name' => 'data_cadastro',
                'required' => true,
                'filters' => array(
                    
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Date',
                        'format' => 'Y-m-d'
                        
                    ),
                ),
            ));
            
            $inputFilter->add(array(
                'name' => 'sobrenome',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ),
                    ),
                ),
            ));

            $inputFilter->add(array(
                'name' => 'data_nascimento',
                'required' => true,
                'filters' => array(
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Date',
                        'format' => 'Y-m-d'
                    ),
                ),
            ));
            
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}
