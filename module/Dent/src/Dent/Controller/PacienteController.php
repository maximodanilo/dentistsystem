<?php

namespace Dent\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Dent\Model\Paciente;          // <-- Add this import
use Dent\Form\PacienteForm;       // <-- Add this import

class PacienteController extends AbstractActionController {

    protected $pacienteTable;

    public function indexAction() {
        $request = $this->getRequest();

        $searchTxt = $request->getPost('searchTxt');

        $pacientes = $searchTxt ? $this->getPacienteTable()->fetchByName($searchTxt) : $this->getPacienteTable()->fetchAll();

        return new ViewModel(array(
            'pacientes' => $pacientes,
            'search' => $searchTxt
        ));
    }

    public function createAction() {
        $form = new PacienteForm();
        $form->get('submit')->setValue('Create');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $paciente = new Paciente();
            $form->setInputFilter($paciente->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $paciente->exchangeArray($form->getData());
                $this->getPacienteTable()->savePaciente($paciente);

                // Redirect to list of pacientes
                return $this->redirect()->toRoute('paciente');
            }
        }
        return array('form' => $form);
    }

    public function updateAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('paciente', array(
                        'action' => 'create'
            ));
        }

        try {
            $paciente = $this->getPacienteTable()->getPaciente($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute('paciente', array(
                        'action' => 'index'
            ));
        }

        $form = new PacienteForm();
        $form->bind($paciente);
        $form->get('submit')->setAttribute('value', 'Update');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($paciente->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getPacienteTable()->savePaciente($paciente);

                // Redirect to list of pacientes
                return $this->redirect()->toRoute('paciente');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('paciente');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getPacienteTable()->deletePaciente($id);
            }

            // Redirect to list of pacientes
            return $this->redirect()->toRoute('paciente');
        }

        return array(
            'id' => $id,
            'paciente' => $this->getPacienteTable()->getPaciente($id)
        );
    }
    
    public function teste(){
        return null;
    }
    
    public function getPacienteTable() {
        if (!$this->pacienteTable) {
            $sm = $this->getServiceLocator();
            $this->pacienteTable = $sm->get('Dent\Model\PacienteTable');
        }
        return $this->pacienteTable;
    }

}
