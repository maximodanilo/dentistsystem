<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pessoa
 *
 * @author Danilo Note
 */

namespace Dent\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Dent\Model\Pessoa;          // <-- Add this import
use Dent\Form\PessoaForm;       // <-- Add this import

class PessoaController extends AbstractActionController {

    protected $pessoaTable;

    public function indexAction() {
        return new ViewModel(array(
            'pessoas' => $this->getPessoaTable()->fetchAll(),
        ));
    }

    public function createAction() {
        $form = new PessoaForm();
        $form->get('submit')->setValue('Create');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $pessoa = new Pessoa();
            $form->setInputFilter($pessoa->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $pessoa->exchangeArray($form->getData());
                $this->getPessoaTable()->savePessoa($pessoa);

                // Redirect to list of pessoas
                return $this->redirect()->toRoute('pessoa');
            }
        }
        return array('form' => $form);
    }

    public function updateAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('pessoa', array(
                        'action' => 'create'
            ));
        }

        try {
            $pessoa = $this->getPessoaTable()->getPessoa($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute('pessoa', array(
                        'action' => 'index'
            ));
        }

        $form = new PessoaForm();
        $form->bind($pessoa);
        $form->get('submit')->setAttribute('value', 'Update');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($pessoa->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getPessoaTable()->savePessoa($pessoa);

                // Redirect to list of pessoas
                return $this->redirect()->toRoute('pessoa');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('pessoa');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getPessoaTable()->deletePessoa($id);
            }

            // Redirect to list of pessoas
            return $this->redirect()->toRoute('pessoa');
        }

        return array(
            'id' => $id,
            'pessoa' => $this->getPessoaTable()->getPessoa($id)
        );
    }

    public function getPessoaTable() {
        if (!$this->pessoaTable) {
            $sm = $this->getServiceLocator();
            $this->pessoaTable = $sm->get('Dent\Model\PessoaTable');
        }
        return $this->pessoaTable;
    }

}
