<?php

namespace Dent\Form;

use Zend\Form\Form;

class PacienteForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('paciente');

        $this->add(array(
            'name' => 'id',
            'type' => 'Text',
            'options' => array(
                'label' => 'Numero cadastro',
            ),
        ));
        $this->add(array(
            'name' => 'data_cadastro',
            'type' => 'Zend\Form\Element\Date',
            'options' => array(
                'label' => 'Data cadastro',
                'format' => 'Y-m-d'
            ),
        ));
        $this->add(array(
            'name' => 'primeiro_nome',
            'type' => 'Text',
            'options' => array(
                'label' => 'Primeiro nome',
            ),
        ));
        $this->add(array(
            'name' => 'sobrenome',
            'type' => 'Text',
            'options' => array(
                'label' => 'Sobrenome',
            ),
        ));
        $this->add(array(
            'name' => 'data_nascimento',
            'type' => 'Zend\Form\Element\Date',
            'options' => array(
                'label' => 'Data nascimento',
                'format' => 'Y-m-d'
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }

}
